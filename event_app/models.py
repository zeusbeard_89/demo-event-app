from __future__ import unicode_literals

from django.db import models
from django.contrib import admin
from uuid import uuid4

def imageuploadeto(instance, filename):
    return '%d/%s' % (instance.speaker_uid, filename)

# Create your models here.
class EventSpeakers(models.Model):
    speaker_uid = models.UUIDField(unique=True, default=uuid4)
    speaker_name = models.CharField(max_length=255, verbose_name="Speaker Name")
    speaker_topic = models.CharField(max_length=255, verbose_name="Speaker Topic", blank=True)
    speaker_image = models.ImageField(blank=True, upload_to=imageuploadeto)
    talk_date = models.DateTimeField(null=True, blank=True)
    speaker_profile = models.ForeignKey('SpeakerProfile', on_delete=models.CASCADE, to_field="profile_uid")
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

    class Meta:
        managed = True
        db_table = "demo_event_speakers"
        verbose_name = "Event Speaker"
        verbose_name_plural = "Event Speakers"


class EventSpeakersAdmin(admin.ModelAdmin):
    list_display = ('speaker_uid', 'speaker_name', 'speaker_topic')


class SpeakerProfile(models.Model):
    profile_uid = models.UUIDField(unique=True, default=uuid4)
    speaker_email = models.CharField(max_length=255, blank=True)
    speaker_location = models.CharField(max_length=255, blank=True)
    speaker_url = models.CharField(max_length=255, blank=True)
    speaker_profile = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

    def __unicode__(self):
        return "{0}".format(self.profile_uid)

    class Meta:
        managed = True
        db_table = "demo_speaker_profiles"
        verbose_name = "Speaker Profile"
        verbose_name_plural = "Speaker Profiles"


class SpeakerProfileAdmin(admin.ModelAdmin):
    list_display = ("profile_uid", "speaker_email", "created_at")


class EventSessions(models.Model):
    session_uid = models.UUIDField(unique=True, default=uuid4)
    event_title = models.CharField(max_length=255)
    event_description = models.TextField(null=True, blank=True)
    event_date = models.DateTimeField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

    class Meta:
        managed = True
        db_table = 'demo_event_sessions'
        verbose_name = "Event Session"
        verbose_name_plural = "Event Sessions"


class EventSessionsAdmin(admin.ModelAdmin):
    list_display = ('session_uid', 'event_title', 'event_date')


class SessionAttendees(models.Model):
    """
    Simple Table no Admin registration or Admin Model is needed
    """
    session_attendee_uid = models.UUIDField(unique=True, default=uuid4)
    attendee = models.ForeignKey('AttendeeDetails', on_delete=models.CASCADE, to_field="attendee_uid")
    session = models.ForeignKey('EventSessions', on_delete=models.CASCADE, to_field="session_uid")
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

    class Meta:
        managed = True
        db_table = "demo_session_attendees"
        verbose_name = "Session Attendee"
        verbose_name_plural = "Session Attendees"


class AttendeeDetails(models.Model):
    """
    A simplified user profile
    """
    attendee_uid = models.UUIDField(unique=True, default=uuid4)
    attendee_name = models.CharField(max_length=255, verbose_name="Name")
    attendee_email = models.CharField(max_length=255, verbose_name="Email")
    attendee_job_title = models.CharField(max_length=150, verbose_name="Job Title")
    attendee_company = models.CharField(max_length=255, verbose_name="Company")
    attendee_profile = models.TextField(verbose_name="Your Profile")
    attendee_image = models.ImageField(verbose_name="Profile Picture")
    attendee_phone = models.CharField(max_length=12, null=True, blank=True, verbose_name="Phone Number")
    attendee_url = models.CharField(max_length=255, null=True, blank=True, verbose_name="Website URL")

    class Meta:
        managed = True
        db_table = 'demo_attendee_details'
        verbose_name = "Attendee"
        verbose_name_plural = "Attendees"


class AttendeeDetailsAdmin(admin.ModelAdmin):
    list_display = ('attendee_uid', 'attendee_name', 'attendee_email', 'attendee_phone')


class EventVenueDetails(models.Model):
    venue_uid = models.UUIDField(unique=True, default=uuid4)
    venue_name = models.CharField(max_length=255)
    venue_description = models.TextField()
    venue_postcode = models.CharField(max_length=10)
    venue_telephone = models.CharField(max_length=12)
    venue_email = models.CharField(max_length=255)

    class Meta:
        managed = True
        db_table = 'demo_venue_details'
        verbose_name = "Venue Detail"
        verbose_name_plural = "Venue Details"


class EventVenueDetailsAdmin(admin.ModelAdmin):
    list_display = ('venue_uid', 'venue_name', 'venue_postcode')