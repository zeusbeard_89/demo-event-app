from django.contrib import admin
from .models import *

admin.site.site_header = "My Awesome Event App"

# Register your models here.
admin.site.register(EventSessions, EventSessionsAdmin)
admin.site.register(EventSpeakers, EventSpeakersAdmin)
admin.site.register(SpeakerProfile, SpeakerProfileAdmin)
admin.site.register(AttendeeDetails, AttendeeDetailsAdmin)
admin.site.register(EventVenueDetails, EventVenueDetailsAdmin)
#TODO Remove after testing
admin.site.register(SessionAttendees)