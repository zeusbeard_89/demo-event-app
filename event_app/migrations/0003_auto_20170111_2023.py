# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-11 20:23
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('event_app', '0002_auto_20170111_2013'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attendeedetails',
            name='attendee_uid',
            field=models.UUIDField(default=uuid.uuid4, unique=True),
        ),
        migrations.AlterField(
            model_name='eventsessions',
            name='session_uid',
            field=models.UUIDField(default=uuid.uuid4, unique=True),
        ),
        migrations.AlterField(
            model_name='eventspeakers',
            name='speaker_uid',
            field=models.UUIDField(default=uuid.uuid4, unique=True),
        ),
        migrations.AlterField(
            model_name='sessionattendees',
            name='session_attendee_uid',
            field=models.UUIDField(default=uuid.uuid4, unique=True),
        ),
        migrations.AlterField(
            model_name='speakerprofile',
            name='profile_uid',
            field=models.UUIDField(default=uuid.uuid4, unique=True),
        ),
    ]
