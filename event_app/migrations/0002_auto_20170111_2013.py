# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-11 20:13
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('event_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventspeakers',
            name='speaker_uid',
            field=models.BigIntegerField(default=uuid.uuid4, unique=True),
        ),
        migrations.AlterField(
            model_name='speakerprofile',
            name='profile_uid',
            field=models.BigIntegerField(default=uuid.uuid4, unique=True),
        ),
    ]
