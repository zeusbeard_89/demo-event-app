from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from rest_framework.urlpatterns import format_suffix_patterns
from event_app import views

urlpatterns = [
    url(r'^speakers/$', views.SpeakerList.as_view()),
    url(r'^speaker/(?P<uid>.*)/details/$', views.SpeakerDetail.as_view()),
    url(r'^sessions/$', views.SessionList.as_view()),
    url(r'^profile/$', views.AttendeeDetails.as_view()),
    url(r'^profile/(?P<id>.*)/update/$', views.AttendeeDetails.as_view()),
    url(r'^attendees/(?P<session>.*>)/$', views.SessionAttendee.as_view()),
    # Post URL
    url(r'^attendees/attend/$', views.SessionAttendee.as_view()),
    url(r'^profile/create/$', views.AttendeeDetails.as_view()),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns)