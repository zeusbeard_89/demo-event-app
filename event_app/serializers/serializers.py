from rest_framework import serializers
from event_app.models import EventSpeakers, SpeakerProfile, EventSessions, AttendeeDetails, SessionAttendees, EventVenueDetails


class SpeakerSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventSpeakers
        fields = ('speaker_uid', 'speaker_name', 'speaker_topic', 'speaker_image', 'talk_date', 'speaker_profile')


class SpeakerProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = SpeakerProfile
        fields = ('profile_uid', 'speaker_email', 'speaker_location', 'speaker_url', 'speaker_profile')


class SessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventSessions
        fields = ('session_uid', 'event_title', 'event_description', 'event_date')


class AttendeeSerializer(serializers.ModelSerializer):
    """
        Removed the UID as default value is set when creating
    """
    class Meta:
        model = AttendeeDetails
        fields = ('attendee_name', 'attendee_email', 'attendee_job_title', 'attendee_company', 'attendee_image')


class SessionAttendeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SessionAttendees
        fields = ('attendee', 'session')


class VenueDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventVenueDetails
        fields = ('venue_name', 'venue_description', 'venue_postcode', 'venue_telephone', 'venue_email')