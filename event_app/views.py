from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404
from event_app.models import *
from event_app.serializers.serializers import *


class SpeakerList(APIView):
    """
    List All Speakers
    """

    def get(self, request, format=None):
        eventspeakers = EventSpeakers.objects.all()
        serializer = SpeakerSerializer(eventspeakers, many=True)
        return Response(serializer.data)


class SpeakerDetail(APIView):
    """
    Gets the chosen speakers profile
    """

    def get(self, request, uid, format=None):
        profile = SpeakerProfile.objects.get(profile_uid=uid)
        serializer = SpeakerProfileSerializer(profile)
        return Response(serializer.data)


class SessionList(APIView):
    """
    List All Sessions
    """

    def get(self, request, format=None):
        sessions = EventSessions.objects.all()
        serializer = SessionSerializer(sessions, many=True)
        return Response(serializer.data)


class AttendeeDetails(APIView):
    """
    View all attendees, edit own profile
    """

    def get(self, request, format=None):
        attendess = AttendeeDetails.objects.all()
        serializer = AttendeeSerializer(attendess, many=True)
        return Response(serializer.data)

        # TODO add profile update / Completed

    def post(self, request, format=None):
        serializer = AttendeeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, id, format=None):
        attendee = AttendeeDetails.objects.get(attendee_uid=id)
        serializer = AttendeeSerializer(attendee, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SessionAttendee(APIView):
    """
    Book sessions and view all specific attendees for session
    """

    def get(self, request, session, format=None):
        sessionattendees = SessionAttendees.objects.filter(session=session)
        serializer = SessionAttendeeSerializer(sessionattendees, many=True)
        return Response(serializer.data)

    # TODO add book function / Complete
    def post(self, request, format=None):
        serializer = SessionAttendeeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VenueDetails(APIView):
    """
    Get the Venue Details
    """

    def get(self, request, format=None):
        eventvenue = EventVenueDetails.objects.all()
        serializer = SpeakerSerializer(eventvenue, many=True)
        return Response(serializer.data)
